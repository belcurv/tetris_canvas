const canvas = document.getElementById('tetris');
const context = canvas.getContext('2d');

context.scale(20,20);

function arenaSweep() {
  let rowCount = 1;
  outer: for (let y = arena.length - 1; y > 0; --y) {
    for (let x = 0; x < arena[y].length; ++x) {
      // if a row is NOT fully populated, continue
      if (arena[y][x] === 0) {
        continue outer;
      }
    }

    // remove row with popualted positions, select it & zero-fill it
    const row = arena.splice(y, 1)[0].fill(0);
    // then put that zeroed row at the top of the arena
    arena.unshift(row);
    // increment y since we removed a row
    ++y;
    player.score += rowCount * 10;
    rowCount *= 2;
  }
}

function collide(arena, player) {
  const [matrix, offset] = [player.matrix, player.pos];
  for (let y = 0; y < matrix.length; ++y) {
    for (let x = 0; x < matrix[y].length; ++x) {
      if (matrix[y][x] !== 0 &&
        (arena[y + offset.y] &&
        arena[y + offset.y][x + offset.x]) !== 0) {
        return true;  // collides
      }
    }
  }
  return false;
}

// creates a 2d nested array of zeros
function createMatrix(width, height) {
  const matrix = [];
  // while height is not zero, we decrement it by 1
  while (height--) {
    matrix.push(new Array(width).fill(0));
  }
  return matrix;
}

// returns 2d nested piece spec'd by param
function createPiece(type) {
  if (type === 'T') {
    return [
      [0, 1, 0],
      [1, 1, 1],
      [0, 0, 0]
    ];
  } else if (type === 'O') {
    return [
      [2, 2],
      [2, 2]
    ];
  } else if (type === 'L') {
    return [
      [0, 0, 3],
      [3, 3, 3],
      [0, 0, 0]
    ];
  } else if (type === 'J') {
    return [
      [4, 0, 0],
      [4, 4, 4],
      [0, 0, 0]
    ];
  } else if (type === 'I') {
    return [
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [5, 5, 5, 5],
      [0, 0, 0, 0]
    ];
  } else if (type === 'S') {
    return [
      [0, 6, 6],
      [6, 6, 0],
      [0, 0, 0]
    ];
  } else if (type === 'Z') {
    return [
      [7, 7, 0],
      [0, 7, 7],
      [0, 0, 0]
    ];
  }
}

// main draw function
// renders the arena at 0,0 and the player at position
function draw() {
  context.fillStyle = '#000';
  context.fillRect(0,0, canvas.width, canvas.height);
  drawMatrix(arena, { x: 0, y: 0 });
  drawMatrix(player.matrix, player.pos);
}

// draws a 2d matrix to the canvas, at spec'd offset, with spec'd color code
function drawMatrix(matrix, offset) {
  matrix.forEach((row, y) => {
    row.forEach((value, x) => {
      if (value !== 0) {
        context.fillStyle = colors[value];
        context.fillRect(x + offset.x, y + offset.y, 1, 1);
      }
    });
  });
}

// copies all position values from player matrix to arena matrix
function merge(arena, player) {
  player.matrix.forEach((row, y) => {
    row.forEach((value, x) => {
      if (value !== 0) {
        arena[y + player.pos.y][x + player.pos.x] = value;
      }
    });
  });
}

// attempts to move player piece down 1
function playerDrop() {
  player.pos.y++;   // move piece down
  if (collide(arena, player)) {   // if we collide while dropping ...
    player.pos.y--;               // move player back up,
    merge(arena, player);         // merge player with arena,
    playerReset();                // start a new piece,
    arenaSweep();                 // check for filled rows,
    updateScore();                // update the score
  }
  dropCounter = 0;  // reset drop counter to get full 1 sec delay after drop.
}

// move the player piece left and right
function playerMove(direction) {
  player.pos.x += direction;
  if (collide(arena, player)) {
    player.pos.x -= direction;
  }
}

// picks a piece at random, resets x/y position, checks for immediate collision
function playerReset() {
  const pieces = 'ILJOTSZ';
  // using bitwise OR to "floor" result of pieces.length * Math.rand():
  player.matrix = createPiece(pieces[pieces.length * Math.random() | 0]);
  player.pos.y = 0;
  player.pos.x = (arena[0].length / 2 | 0) - (player.matrix[0].length / 2 | 0);
  // if we collide immediately after resetting, it means we've filed
  // rubble to the top
  if (collide(arena, player)) {
    arena.forEach(row => row.fill(0));
    player.score = 0;
    updateScore();
  }
}

// attempts to rotate the player
// if colide with either wall, try to find valid position left/right
function playerRotate(direction) {
  const pos = player.pos.x;
  let offset = 1;
  rotate(player.matrix, direction);
  while (collide(arena, player)) {
    player.pos.x += offset;
    // grow offset by 1 in alternating opposite directions
    offset = -(offset + (offset > 0 ? 1 : -1));
    if (offset > player.matrix[0].length) {
      // we've offset beyond the width pf the player, give up.
      rotate(player.matrix, -direction);
      player.pos.x = pos;
      return;
    }
  }
}

// To rotate, we transpose all rows to columns, and then reverse the columns
function rotate(matrix, direction) {
  for (let y = 0; y < matrix.length; y++) {
    for (let x = 0; x < y; x++) {
      // tuple switch
      [matrix[x][y], matrix[y][x]] = [matrix[y][x], matrix[x][y]];
    }
  }
  if (direction > 0) {
    matrix.forEach(row => row.reverse());
  } else {
    matrix.reverse();
  }
}

// set some timing variables
let dropCounter = 0;
let dropInterval = 1000;  // ms
let lastTime = 0;

// the main game loop
function update(time = 0) {
  const deltaTime = time - lastTime;
  
  dropCounter += deltaTime;
  if (dropCounter > dropInterval) {
    playerDrop();
  }
  
  lastTime = time;
  
  draw();
  requestAnimationFrame(update);
}

// renders score to DOM
function updateScore() {
  document.getElementById('score').innerText = player.score;
}

// tetromino colors, referenced by index (eg 1, 2, 7, etc)
const colors = [
  null,
  '#FF0D72',
  '#0DC2FF',
  '#0DFF72',
  '#F538FF',
  '#FF8E0D',
  '#FFE138',
  '#3877FF'
];

// init game board
const arena = createMatrix(12, 20);

// init player
const player = {
  pos: {x: 0, y: 0},
  matrix: null,
  score: 0
};

// bind keydown events
document.addEventListener('keydown', (event) => {
  if (event.keyCode === 37) {
    playerMove(-1);
  } else if (event.keyCode === 39) {
    playerMove(1);
  } else if (event.keyCode === 40) {
    playerDrop();
  } else if (event.keyCode === 81) {
    playerRotate(-1);
  } else if (event.keyCode === 87) {
    playerRotate(1);
  }
});

// init game
playerReset();
updateScore();
update();
